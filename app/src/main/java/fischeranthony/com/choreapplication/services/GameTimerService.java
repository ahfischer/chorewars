package fischeranthony.com.choreapplication.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.activities.MainActivity;

public class GameTimerService extends IntentService {

    public static final String EXTRA_VICTORY = "fischeranthony.com.choreapplication.EXTRA_VICTORY";

    public GameTimerService() {
        super("GameTimerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {



        displayNotification();
    }

    private void displayNotification() {
        NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Setup small style
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_player_stats_24dp);
        builder.setContentTitle("Victory!");
        builder.setContentTitle("Watch Battle!");
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

        // Open Victory Screen
        Intent victoryIntent = new Intent(getApplicationContext(), MainActivity.class);
        victoryIntent.putExtra(EXTRA_VICTORY, "Victory");
        PendingIntent openVictoryScreen = PendingIntent.getActivity(this, 1, victoryIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(openVictoryScreen);

        // Setup big style
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
        style.setBigContentTitle("Victory!");
        style.setSummaryText("Watch Battle!");
        style.bigText("Everyone Fights!");
        builder.setStyle(style);

        // Build notification
        Notification notification = builder.build();
        int EXPANDED_NOTIFICATION = 0x01001;
        mgr.notify(EXPANDED_NOTIFICATION, notification);
    }
}
