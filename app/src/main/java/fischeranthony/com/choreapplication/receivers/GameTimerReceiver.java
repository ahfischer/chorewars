package fischeranthony.com.choreapplication.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import fischeranthony.com.choreapplication.services.GameTimerService;

public class GameTimerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent gameTimerServiceIntent = new Intent(context, GameTimerService.class);
        context.startService(gameTimerServiceIntent);
    }
}
