package fischeranthony.com.choreapplication.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.fragments.VictoryFragment;

public class VictoryActivity extends AppCompatActivity {

    public static String EXTRA_PLAYER_NAME = "fischeranthony.com.choreapplication.EXTRA_PLAYER_NAME";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup actionbar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_launcher);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        // Display Fragment
        VictoryFragment victoryFragment = VictoryFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.container, victoryFragment, VictoryFragment.TAG)
                .commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.restart_menu, menu);

        return true;
    }
}
