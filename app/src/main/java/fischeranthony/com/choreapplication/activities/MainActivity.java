package fischeranthony.com.choreapplication.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;

import fischeranthony.com.choreapplication.fragments.PlayerSelectionFragment;
import fischeranthony.com.choreapplication.fragments.VictoryFragment;
import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.receivers.GameTimerReceiver;
import fischeranthony.com.choreapplication.services.GameTimerService;
import fischeranthony.com.choreapplication.utils.DataUtil;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    private DatabaseReference mUser;
    private DatabaseReference mGame;

    private String mGroupName;
    private String mPlayerName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if (intent.hasExtra(SignUpActivity.EXTRA_GROUP_NAME)) {
            mGroupName = intent.getStringExtra(SignUpActivity.EXTRA_GROUP_NAME);
        } else if (intent.hasExtra(LoginActivity.EXTRA_PLAYER_NAME)) {
            mPlayerName = intent.getStringExtra(LoginActivity.EXTRA_PLAYER_NAME);
        }

        // Setup actionbar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_launcher);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // AUTOMATICALLY LOGS YOU IN IF YOU NEVER LOG OUT
        if (mFirebaseUser == null) {

            // Only save total chore list if not signed in
            // OR JUST DO DATABASE PULL FOR ALL CHORES THEN ONLY SAVE REMAINING CHORES
            ArrayList<Chore> chores = new ArrayList<>();
            chores.add(new Chore("Clean the Litter Box", 4));
            chores.add(new Chore("Dishes", 2));
            chores.add(new Chore("Dust", 2));
            chores.add(new Chore("Laundry", 2));
            chores.add(new Chore("Organize", 2));
            chores.add(new Chore("Paint the Floor", 8));
            chores.add(new Chore("Rake Leaves", 6));
            chores.add(new Chore("Shovel Snow", 6));
            chores.add(new Chore("Vacuum", 4));
            chores.add(new Chore("Walk the Dog", 4));
            chores.add(new Chore("Wash Dishes", 2));

            DataUtil.saveAllChoreData(chores, MainActivity.this);

            setupGameTimer();

            // Not logged in, launch the Log In activity
            loadLogInView();
        } else {

            mUserId = mFirebaseUser.getUid();

            if (intent.hasExtra(SignUpActivity.EXTRA_GROUP_NAME)
                    && intent.hasExtra(SignUpActivity.EXTRA_PLAYERS)) {

                ArrayList<Player> players = (ArrayList<Player>) intent.getSerializableExtra(SignUpActivity.EXTRA_PLAYERS);

                // Setup Group
                mDatabase.child("users").child(mUserId).child(mGroupName);
                mGame = mDatabase.child("users").child(mUserId).child(mGroupName);

                // Setup All Chores
                setupAllChores(mGame);

                // Setup All Players
                setupAllPlayers(mGame, players);
                // make each chore a child of chore lists, then setValue to complete or incomplete

                PlayerSelectionFragment playerSelectionFragment = PlayerSelectionFragment.newInstance();
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, playerSelectionFragment, PlayerSelectionFragment.TAG)
                        .commit();

            } else if (intent.hasExtra(GameTimerService.EXTRA_VICTORY)) {
                // Start battle scene
                loadVictoryScene();
            } else {
                PlayerSelectionFragment playerSelectionFragment = PlayerSelectionFragment.newInstance();
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, playerSelectionFragment, PlayerSelectionFragment.TAG)
                        .commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();
            loadLogInView();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // on result for victory activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        PlayerSelectionFragment playerSelectionFragment = PlayerSelectionFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.container, playerSelectionFragment, PlayerSelectionFragment.TAG)
                .commit();

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadLogInView() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void loadSignUpView() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void loadVictoryScene() {
        Intent intent = new Intent(this, VictoryActivity.class);
        startActivityForResult(intent, 0x02002);
    }

    // Set complete chore list with values
    private void setupAllChores(DatabaseReference game) {
        game.child("Complete Chore List").child("Clean the Litter Box").setValue("4 points");
        game.child("Complete Chore List").child("Dishes").setValue("2 points");
        game.child("Complete Chore List").child("Dust").setValue("2 points");
        game.child("Complete Chore List").child("Laundry").setValue("2 points");
        game.child("Complete Chore List").child("Organize").setValue("2 points");
        game.child("Complete Chore List").child("Paint the Floor").setValue("8 points");
        game.child("Complete Chore List").child("Rake Leaves").setValue("6 points");
        game.child("Complete Chore List").child("Shovel Snow").setValue("6 points");
        game.child("Complete Chore List").child("Vacuum").setValue("4 points");
        game.child("Complete Chore List").child("Wash Dishes").setValue("2 points");
        game.child("Complete Chore List").child("Walk the Dog").setValue("4 points");
    }

    private void setupAllPlayers(DatabaseReference game, ArrayList<Player> players) {
        for (int i = 0; i < players.size(); i++) {
            game.child("Players").child(players.get(i).getmPlayerName());
        }
    }

    // CHAD, CHANGE THIS TO FORCE THE NOTIFICATION TO SEND EARLIER
    private void setupGameTimer() {

        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(MainActivity.this, GameTimerReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);

        // CURRENTLY SET TO 8:30, CHANGE TO WHATEVER YOU NEED TO TEST
        // Set the alarm to start at 8:30 p.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 20);
        calendar.set(Calendar.MINUTE, 30);

        // Run once a day at the same time
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, alarmIntent);
    }

}
