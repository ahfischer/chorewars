package fischeranthony.com.choreapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.utils.DataUtil;

public class SignUpActivity extends AppCompatActivity {

    public static String EXTRA_GROUP_NAME = "fischeranthony.com.choreapplication.EXTRA_GROUP_NAME";
    public static String EXTRA_PLAYERS = "fischeranthony.com.choreapplication.EXTRA_PLAYERS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // Initialize FirebaseAuth
        final FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();

        final ArrayList<Player> players = new ArrayList<>();

        final EditText groupNameEditText = (EditText)findViewById(R.id.groupNameField);
        final EditText passwordEditText = (EditText)findViewById(R.id.passwordField);
        final EditText emailEditText = (EditText)findViewById(R.id.emailField);
        final EditText addPlayerEditText = (EditText)findViewById(R.id.addPlayerField);
        final Button addPlayerButton = (Button)findViewById(R.id.addPlayerButton);
        final TextView playerListTextView = (TextView)findViewById(R.id.playerListTextView);
        final Button signUpButton = (Button)findViewById(R.id.signupButton);

        // Add all players to display
        addPlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                players.add(new Player(addPlayerEditText.getText().toString()));

                String playersString = "";
                for (int i = 0; i < players.size(); i++) {
                    playersString += ( "\n"+players.get(i).getmPlayerName() );
                }

                addPlayerEditText.setText("");
                playerListTextView.setText(playersString);
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = passwordEditText.getText().toString();
                String email = emailEditText.getText().toString();

                password = password.trim();
                email = email.trim();

                // Check that all fields are filled
                if (password.isEmpty() || email.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                    builder.setMessage(R.string.signup_error_message)
                            .setTitle(R.string.signup_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {

                    DataUtil.saveAllPlayerData(players, SignUpActivity.this);

                    // Sign user in
                    mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                                intent.putExtra(EXTRA_GROUP_NAME, groupNameEditText.getText().toString());
                                intent.putExtra(EXTRA_PLAYERS, players);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
                                builder.setMessage(task.getException().getMessage())
                                        .setTitle(R.string.login_error_title)
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
                }
            }
        });
    }

}
