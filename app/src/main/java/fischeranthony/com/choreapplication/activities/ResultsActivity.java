package fischeranthony.com.choreapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.fragments.PlayerSelectionFragment;
import fischeranthony.com.choreapplication.fragments.ResultsFragment;
public class ResultsActivity extends AppCompatActivity {

    public static String EXTRA_PLAYER_NAME = "fischeranthony.com.choreapplication.EXTRA_PLAYER_NAME";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup actionbar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_launcher);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(PlayerSelectionFragment.EXTRA_PLAYER)) {
                // Display Fragment
                ResultsFragment resultsFragment = ResultsFragment.newInstance("Weekly");
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, resultsFragment, ResultsFragment.TAG)
                        .commit();
            } else {
                // Display Fragment
                ResultsFragment resultsFragment = ResultsFragment.newInstance();
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, resultsFragment, ResultsFragment.TAG)
                        .commit();
            }
        }
    }
}
