package fischeranthony.com.choreapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import fischeranthony.com.choreapplication.fragments.ChoreListFragment;
import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.fragments.PlayerSelectionFragment;

public class ChoreListActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup actionbar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.mipmap.ic_launcher);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(PlayerSelectionFragment.EXTRA_PLAYER)) {

                //Player player = (Player) intent.getSerializableExtra(PlayerSelectionFragment.EXTRA_PLAYER);
                String playerName = intent.getStringExtra(PlayerSelectionFragment.EXTRA_PLAYER);

                // Display Fragment
                ChoreListFragment choreListFragment = ChoreListFragment.newInstance(playerName);
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, choreListFragment, ChoreListFragment.TAG)
                        .commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);

        return true;
    }
}
