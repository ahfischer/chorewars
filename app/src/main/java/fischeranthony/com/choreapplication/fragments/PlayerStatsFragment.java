package fischeranthony.com.choreapplication.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.activities.LoginActivity;
import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.utils.DataUtil;


public class PlayerStatsFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "PlayerStatsFragment.TAG";

    private static final String ARG_PLAYER_NAME = "com.fullsail.android.ARG_PLAYER_NAME";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    private ArrayList<Player> mPlayers;
    private Player mCurrentPlayer;
    private int mCounter = 0;

    public static PlayerStatsFragment newInstance(String nameArg) {
        PlayerStatsFragment playerStatsFragment = new PlayerStatsFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(ARG_PLAYER_NAME, nameArg);
        playerStatsFragment.setArguments(arg);
        return playerStatsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserId = mFirebaseUser.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_player_stats, container, false);

        Bundle arguments = getArguments();
        if (arguments != null) {

            final String playerName = arguments.getString(ARG_PLAYER_NAME);

            view.findViewById(R.id.avatarChangeLeft).setOnClickListener(this);
            view.findViewById(R.id.avatarChangeRight).setOnClickListener(this);

            ImageView avatarImageView = (ImageView) view.findViewById(R.id.avatarImageView);

            TextView strengthTextView = (TextView) view.findViewById(R.id.strengthStatTextView);
            TextView speedTextView = (TextView) view.findViewById(R.id.speedStatTextView);

            mPlayers = DataUtil.loadAllPlayerData(getActivity());
            for (int i = 0; i < mPlayers.size(); i++) {
                if (mPlayers.get(i).getmPlayerName().equals(playerName)) {

                    if (mPlayers.get(i).getmAvatar() != -1) {
                        avatarImageView.setImageResource(mPlayers.get(i).getmAvatar());
                    }

                    //ArrayList<Chore> completedChores = mPlayers.get(i).getmCompletedChores();
                    mCurrentPlayer = mPlayers.get(i);
//                    if (completedChores != null) {
//                        int points = 0;
//                        for (int j = 0; j < completedChores.size(); j++) {
//                            points += completedChores.get(j).getmChorePoints();
//                        }
//
//                        String strengthStat = "Strength: " + String.valueOf(points);
//                        strengthTextView.setText(strengthStat);
//
//                    } else {
//                        strengthTextView.setText("Complete Chores to Gain Strength!");
//                    }

                    int points = mPlayers.get(i).getmPoints();
                    if (points != 0) {
                        String strengthStat = "Strength: " + String.valueOf(points);
                        strengthTextView.setText(strengthStat);
                    } else {
                        strengthTextView.setText("Complete Chores to Gain Strength!");
                    }
                }

            }
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        View view = getView();

        if (view != null) {
            ImageView avatarImageView = (ImageView) view.findViewById(R.id.avatarImageView);

            ArrayList<Integer> avatarImages = new ArrayList<>();
            avatarImages.add(R.drawable.avatar1);
            avatarImages.add(R.drawable.avatar2);
            avatarImages.add(R.drawable.avatar3);
            avatarImages.add(R.drawable.avatar4);
            avatarImages.add(R.drawable.avatar5);
            avatarImages.add(R.drawable.avatar6);

            // Cycle through images for avatars
            if (v.getId() == R.id.avatarChangeLeft) {
                if (mCounter > 0) {
                    mCounter -= 1;
                } else {
                    mCounter = avatarImages.size() - 1;
                }
                avatarImageView.setImageResource(avatarImages.get(mCounter));
                mCurrentPlayer.setmAvatar(avatarImages.get(mCounter));
                for (int i = 0; i < mPlayers.size(); i++) {
                    if (mPlayers.get(i).getmPlayerName().equals(mCurrentPlayer.getmPlayerName())) {
                        // Replace current player avatar with new avatar
                        mPlayers.get(i).setmAvatar(mCurrentPlayer.getmAvatar());
                        DataUtil.saveAllPlayerData(mPlayers, getActivity());
                    }
                }
            }

            if (v.getId() == R.id.avatarChangeRight) {
                if (mCounter < avatarImages.size() - 1) {
                    mCounter += 1;
                } else {
                    mCounter = 0;
                }
                avatarImageView.setImageResource(avatarImages.get(mCounter));
                mCurrentPlayer.setmAvatar(avatarImages.get(mCounter));
                for (int i = 0; i < mPlayers.size(); i++) {
                    if (mPlayers.get(i).getmPlayerName().equals(mCurrentPlayer.getmPlayerName())) {
                        // Replace current player avatar with new avatar
                        mPlayers.get(i).setmAvatar(mCurrentPlayer.getmAvatar());
                        DataUtil.saveAllPlayerData(mPlayers, getActivity());
                    }
                }
            }
        }
    }
}
