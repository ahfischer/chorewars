package fischeranthony.com.choreapplication.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.utils.DataUtil;

public class ResultsFragment extends Fragment {

    public static final String TAG = "ResultsFragment.TAG";

    private static final String ARG_PLAYER_NAME = "com.fullsail.android.ARG_PLAYER_NAME";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    public static ResultsFragment newInstance() {
        return new ResultsFragment();
    }

    public static ResultsFragment newInstance(String nameArg) {
        ResultsFragment playerStatsFragment = new ResultsFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(ARG_PLAYER_NAME, nameArg);
        playerStatsFragment.setArguments(arg);
        return playerStatsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserId = mFirebaseUser.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_results, container, false);

        final ViewGroup linearLayout = (ViewGroup) view.findViewById(R.id.fragment_results);

        Bundle bundle = getArguments();
        if (bundle != null) {

            ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
            for (int i = 0; i < players.size(); i++) {

                ImageView image = new ImageView(getActivity());
                image.setImageResource(players.get(i).getmAvatar());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
                layoutParams.gravity = Gravity.CENTER;
                image.setLayoutParams(layoutParams);
                linearLayout.addView(image);


                TextView textView = new TextView(getActivity());
                textView.setText(players.get(i).getmPlayerName());
                textView.setGravity(Gravity.CENTER);
                linearLayout.addView(textView);

                TextView textView1 = new TextView(getActivity());
                textView1.setText(String.valueOf(players.get(i).getmPoints()));
                textView1.setGravity(Gravity.CENTER);
                linearLayout.addView(textView1);
            }
        } else {
            ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
            for (int i = 0; i < players.size(); i++) {

                ImageView image = new ImageView(getActivity());
                image.setImageResource(players.get(i).getmAvatar());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
                layoutParams.gravity = Gravity.CENTER;
                image.setLayoutParams(layoutParams);
                linearLayout.addView(image);


                TextView textView = new TextView(getActivity());
                textView.setText(players.get(i).getmPlayerName());
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(Color.WHITE);
                linearLayout.addView(textView);

                TextView textView1 = new TextView(getActivity());
                textView1.setText(String.valueOf(players.get(i).getmPoints()));
                textView1.setGravity(Gravity.CENTER);
                textView1.setTextColor(Color.WHITE);
                linearLayout.addView(textView1);
            }
        }

        return view;
    }
}
