package fischeranthony.com.choreapplication.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.utils.DataUtil;

public class VictoryFragment extends Fragment {

    public static final String TAG = "VictoryActivity.TAG";

    private static final String ARG_PLAYER_NAME = "com.fullsail.android.ARG_PLAYER_NAME";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    public static VictoryFragment newInstance() {
        return new VictoryFragment();
    }

    public static VictoryFragment newInstance(String nameArg) {
        VictoryFragment playerStatsFragment = new VictoryFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(ARG_PLAYER_NAME, nameArg);
        playerStatsFragment.setArguments(arg);
        return playerStatsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserId = mFirebaseUser.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_victory, container, false);

        final ViewGroup linearLayout = (ViewGroup) view.findViewById(R.id.fragment_victory);

        ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
        for (int i = 0; i < players.size(); i++) {
            ImageView image = new ImageView(getActivity());
            image.setImageResource(players.get(i).getmAvatar());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
            layoutParams.gravity = Gravity.CENTER;
            image.setLayoutParams(layoutParams);
            linearLayout.addView(image);

            Animation rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_rotate);
            image.startAnimation(rotate);

            if (i == players.size()-1) {
                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        ArrayList<String> playerScores = new ArrayList<>();

                        ArrayList<Player> mPlayers = DataUtil.loadAllPlayerData(getActivity());
                        for (int i = 0; i < mPlayers.size(); i++) {
                            ArrayList<Chore> completedChores = mPlayers.get(i).getmCompletedChores();
                            Player mCurrentPlayer = mPlayers.get(i);
                            int points = 0;
                            if (completedChores != null) {
                                for (int j = 0; j < completedChores.size(); j++) {
                                    points += completedChores.get(j).getmChorePoints();
                                    mCurrentPlayer.setmPoints(points);
                                }
                            }
                            TextView textView = new TextView(getActivity());
                            String playerData = mCurrentPlayer.getmPlayerName()+" Points: "+ mCurrentPlayer.getmPoints();
                            playerScores.add(playerData);
                            textView.setText(playerData);
                            textView.setGravity(Gravity.CENTER);
                            textView.setTextColor(Color.WHITE);
                            linearLayout.addView(textView);
                        }

                        if (mPlayers.size() > 0) {
                            int highest = mPlayers.get(0).getmPoints();
                            int highestIndex = 0;

                            for (int j = 1; j < mPlayers.size(); j++){
                                int currentValue = mPlayers.get(j).getmPoints();
                                if (currentValue > highest) {
                                    highest = currentValue;
                                    highestIndex = j;
                                }
                            }

                            TextView textView = new TextView(getActivity());
                            String data = mPlayers.get(highestIndex).getmPlayerName() + " Wins!";
                            textView.setText(data);
                            linearLayout.addView(textView);
                        }

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }

        return view;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_restart) {

            //final ArrayList<Player> players = new ArrayList<>();

            // Reset Database
            mDatabase.child("users").child(mUserId).child("Players").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() != null) {
                        //user exists, do something

                        ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
                        int iterator = 0;
                        for(final DataSnapshot dsp : snapshot.getChildren()){
                            players.set(iterator, new Player(dsp.getKey()));
                            dsp.getRef().removeValue();
                            iterator++;
                        }

                        // Reset all player data
                        DataUtil.saveAllPlayerData(players, getActivity());

                        ArrayList<Chore> chores = new ArrayList<>();
                        chores.add(new Chore("Clean the Litter Box", 4));
                        chores.add(new Chore("Dishes", 2));
                        chores.add(new Chore("Dust", 2));
                        chores.add(new Chore("Laundry", 2));
                        chores.add(new Chore("Organize", 2));
                        chores.add(new Chore("Paint the Floor", 8));
                        chores.add(new Chore("Rake Leaves", 6));
                        chores.add(new Chore("Shovel Snow", 6));
                        chores.add(new Chore("Vacuum", 4));
                        chores.add(new Chore("Walk the Dog", 4));
                        chores.add(new Chore("Wash Dishes", 2));

                        DataUtil.saveAllChoreData(chores, getActivity());

                        getActivity().setResult(Activity.RESULT_OK);
                        getActivity().finish();

                    } else {
                        //user does not exist, do something else
                    }
                }
                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("Failed to read value.", error.toException());
                }
            });

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
