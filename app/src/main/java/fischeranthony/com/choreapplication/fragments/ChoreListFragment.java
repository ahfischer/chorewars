package fischeranthony.com.choreapplication.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.utils.DataUtil;

// ALARM MANAGER TO CALL GAMES EVERY DAY/WEEK WITH BROADCAST

public class ChoreListFragment extends Fragment {

    public static final String TAG = "ChoreListFragment.TAG";

    private static final String ARG_PLAYER_NAME = "com.fullsail.android.ARG_PLAYER_NAME";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    private ArrayList<Chore> mChores;
    private ArrayList<Chore> mChosenChores = new ArrayList<>();
    private ArrayList<Chore> mChosenChore = new ArrayList<>();
    private ArrayList<Chore> mCompletedChores = new ArrayList<>();
    private ArrayList<Chore> mChoresLeft = new ArrayList<>();

    private ArrayList<String> mGroupMembers = new ArrayList<>();

    private String mPlayerName;

    public static ChoreListFragment newInstance(String nameArg) {
        ChoreListFragment choreListFragment = new ChoreListFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(ARG_PLAYER_NAME, nameArg);
        choreListFragment.setArguments(arg);
        return choreListFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserId = mFirebaseUser.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_chore_list, container, false);

        mChores = new ArrayList<>();

        final TextView chosenChorePoints = (TextView) view.findViewById(R.id.chosenChorePoints);

        //Set up ListView
        final ListView completeChoreListView = (ListView) view.findViewById(R.id.completeChoreListView);
        final ListView playerChoreListView = (ListView) view.findViewById(R.id.playerChoreListView);
        final ArrayAdapter<String> completeChoreAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1);
        final ArrayAdapter<String> playerChoreAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1);

        // Setup Complete Chore List
        mChores = DataUtil.loadAllChoreData(getActivity());

        for (int i = 0; i < mChores.size(); i++) {
            completeChoreAdapter.add(mChores.get(i).getmChoreName());
        }

        completeChoreListView.setAdapter(completeChoreAdapter);

        Bundle arguments = getArguments();
        if (arguments != null) {

            final String playerName = arguments.getString(ARG_PLAYER_NAME);

            if (playerName != null) {
                mPlayerName = playerName;

                // Load and display all previously chosen chores
                final ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
                for (int i = 0; i < players.size(); i++) {
                    if (players.get(i).getmPlayerName().equals(playerName)) {
                        mChosenChores = players.get(i).getmChoreList();
                        if (mChosenChores != null) {
                            for (int j = 0; j < mChosenChores.size(); j++) {
                                playerChoreAdapter.add(mChosenChores.get(j).getmChoreName());
                                playerChoreListView.setAdapter(playerChoreAdapter);
                            }
                            String chosenChorePointString = "Chosen Chores: "+String.valueOf(tallyPoints(mChosenChores))+" Points / 10 Points";
                            chosenChorePoints.setText(chosenChorePointString);
                        }
                    }
                }

                mDatabase.child("users").child(mUserId).child(playerName).child("Chore Lists").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.getValue() != null) {
                            //user exists, do something

                            mDatabase.child("users").child(mUserId).child(playerName).child("Chore Lists").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    mGroupMembers.add(dataSnapshot.getKey());
                                    for (int i = 0; i < mGroupMembers.size(); i++) {
                                        String choreName = dataSnapshot.child("users").child(mUserId).child("Players").child(playerName).child("Chore Lists").getKey();
                                        int chorePoints = Integer.parseInt(dataSnapshot.child("users").child(mUserId).child("Players").child(playerName).child("Chore Lists").child(choreName).getValue().toString());
                                        Chore chore = new Chore(choreName, chorePoints);
                                        mChosenChores.add(chore);
                                        playerChoreAdapter.add(mChosenChores.get(i).getmChoreName());
                                        playerChoreListView.setAdapter(playerChoreAdapter);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError error) {
                                    // Failed to read value
                                    Log.w("Failed to read value.", error.toException());
                                }
                            });

                        } else {
                            //user does not exist, do something else
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("Failed to read value.", error.toException());
                    }
                });

                completeChoreListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        //mChosenChore = new ArrayList<>();
                        if (mChosenChores == null) {
                            mChosenChores = new ArrayList<>();
                        }
                        mChosenChore.add(mChores.get(position));
                        int points = tallyPoints(mChosenChore)+tallyPoints(mChosenChores);
                        if (points <= 10) {
                            mDatabase.child("users").child(mUserId).child("Players").child(playerName).child("Chore Lists").child(mChores.get(position).getmChoreName()).setValue(0);
                            playerChoreAdapter.add(mChores.get(position).getmChoreName());
                            mChosenChores.add(mChores.get(position));
                            playerChoreListView.setAdapter(playerChoreAdapter);

                            ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
                            for (int i = 0; i < players.size(); i++) {
                                if (players.get(i).getmPlayerName().equals(playerName)) {
                                    players.get(i).setmChoreList(mChosenChores);
                                    //setcompletedchores
                                }
                            }
                            DataUtil.saveAllPlayerData(players, getActivity());

                            String chosenChorePointString = "Chosen Chores: "+String.valueOf(tallyPoints(mChosenChores))+" Points / 10 Points";
                            chosenChorePoints.setText(chosenChorePointString);

                            // remove from mChores;
                            completeChoreAdapter.remove(mChores.get(position).getmChoreName());
                            mChores.remove(mChores.get(position));
                            completeChoreListView.setAdapter(completeChoreAdapter);

                            // SAVE ALL CHORES HERE TO TELL OTHER MEMBERS THAT THE CHOSEN CHORES ARE NO LONGER AVAILABLE
                            // DO NOT NEED TO UPDATE DATABASE, SINCE IT IS LOCALLY SAVED
                            DataUtil.saveAllChoreData(mChores, getActivity());
                        } else {
                            Toast.makeText(getActivity(), "Chores must add up to 10 points", Toast.LENGTH_SHORT).show();
                        }
                        mChosenChore = new ArrayList<>();
                    }
                });

                playerChoreListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        mDatabase.child("users").child(mUserId).child("Players").child(playerName).child("Chore Lists").child(mChosenChores.get(position).getmChoreName()).setValue(assignPoints(mChosenChores.get(position).getmChoreName()));

                        if (mCompletedChores.size() == 0 && mChores.size() >= position) {
                            mCompletedChores.add(mChores.get(position));
                            ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
                            if (players != null) {
                                for (int j = 0; j < players.size(); j++) {
                                    if (players.get(j).getmPlayerName().equals(playerName)) {
                                        players.get(j).setmCompletedChores(mCompletedChores);
                                    }
                                }
                            }
                        } else {
                            // If chore has not already been completed
                            for (int i = 0; i < mCompletedChores.size(); i++) {
                                if (!mCompletedChores.get(i).getmChoreName().equals(mChores.get(position).getmChoreName())) {
                                    mCompletedChores.add(mChores.get(position));
                                    ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
                                    if (players != null) {
                                        for (int j = 0; j < players.size(); j++) {
                                            if (players.get(j).getmPlayerName().equals(playerName)) {
                                                players.get(j).setmCompletedChores(mCompletedChores);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //DataUtil.saveAllPlayerData(players, getActivity());

                    }
                });

                //Use Firebase to populate the list.
                mDatabase.child("users").child(mUserId).child(playerName).child("Chore Lists").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        completeChoreAdapter.remove(dataSnapshot.getKey());
                        completeChoreListView.setAdapter(completeChoreAdapter);

                        playerChoreAdapter.add(dataSnapshot.getKey());
                        playerChoreListView.setAdapter(playerChoreAdapter);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        completeChoreAdapter.remove(dataSnapshot.getKey());
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }

        return view;
    }

    private int tallyPoints(ArrayList<Chore> chores) {
        int points = 0;
        for (Chore chore: chores) {
            points+=chore.getmChorePoints();
        }
        return points;
    }

    private int assignPoints(String choreName) {
        switch (choreName) {
            case "Clean the Litter Box":
                return 4;
            case "Dishes":
                return 2;
            case "Dust":
                return 2;
            case "Laundry":
                return 2;
            case "Organize":
                return 2;
            case "Paint the Floor":
                return 8;
            case "Rake Leaves":
                return 6;
            case "Shovel Snow":
                return 6;
            case "Vacuum":
                return 4;
            case "Walk the Dog":
                return 4;
            case "Wash Dishes":
                return 2;
            default:
                return 0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).getmPlayerName().equals(mPlayerName)) {

                    ArrayList<Chore> chosenChores = players.get(i).getmChoreList();

                    // Grab original chore list, add any additional chores
                    if (chosenChores == null) {
                        chosenChores = new ArrayList<>();
                    }

                    if (chosenChores.size() > 0 || mChosenChores.size() > 0) {
                        for (int j = 0; j < mChosenChores.size(); j++) {

                            if (chosenChores.size() > 0 && mChosenChores.size() > 0
                                    && chosenChores.size() > j && mChosenChores.size() > j) {
                                if (!mChosenChores.get(j).getmChoreName().equals(chosenChores.get(j).getmChoreName())) {
                                    chosenChores.add(mChosenChores.get(j));
                                }
                            } else {
                                chosenChores.add(mChosenChores.get(j));
                            }
                        }
                        players.get(i).setmChoreList(chosenChores);
                    }


                    // Grab original completed chore list, add any additional completed chores
                    ArrayList<Chore> completedChores = players.get(i).getmCompletedChores();
                    if (completedChores == null) {
                        completedChores = new ArrayList<>();
                    }
                    if (completedChores.size() > 0) {
                        for (int j = 0; j < mCompletedChores.size(); j++) {
                            if (!mCompletedChores.get(j).getmChoreName().equals(completedChores.get(j).getmChoreName())) {
                                completedChores.add(mCompletedChores.get(j));
                            }
                        }
                        players.get(i).setmCompletedChores(completedChores);

                        int points = 0;
                        for (int k = 0; k < completedChores.size(); k++) {
                                points += completedChores.get(k).getmChorePoints();
                        }
                        players.get(i).setmPoints(points);

                    // CHAD, THIS IS THE ONLY THING I CHANGED TO MAKE THE POINT SYSTEM WORK
                    // I CHANGED THE POINT SYSTEM SLIGHTLY BEFORE THE DEADLINE YESTERDAY AND
                    // I COULD NOT FIGURE THIS OUT LAST NIGHT, AS I WAS PANICKING DUE TO THE ISSUE
                    // JUST BEFORE THE DEADLINE
                    } else {
                        // if there were no completed chores already, grab completed chores then do points
                        if (mCompletedChores != null) {
                            players.get(i).setmCompletedChores(mCompletedChores);
                        }

                        int points = 0;
                        for (int k = 0; k < players.get(i).getmCompletedChores().size(); k++) {
                            points += players.get(i).getmCompletedChores().get(k).getmChorePoints();
                        }
                        players.get(i).setmPoints(points);
                    }
                    // THIS ENDS THE CHANGE
                }

            }
            DataUtil.saveAllPlayerData(players, getActivity());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
