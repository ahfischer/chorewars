package fischeranthony.com.choreapplication.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.activities.LoginActivity;
import fischeranthony.com.choreapplication.activities.MainMenuActivity;
import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.objects.Player;
import fischeranthony.com.choreapplication.utils.DataUtil;

public class PlayerSelectionFragment extends Fragment {

    public static final String TAG = "PlayerSelectionFragment.TAG";

    private static final String ARG_PLAYER_NAME = "com.fullsail.android.ARG_PLAYER_NAME";

    public static final String EXTRA_PLAYER = "com.fullsail.android.EXTRA_PLAYER";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    private ArrayList<String> mGroupMembers = new ArrayList<>();
    private ArrayList<Player> mPlayers = new ArrayList<>();

    public static PlayerSelectionFragment newInstance() {
        PlayerSelectionFragment playerSelectionFragment = new PlayerSelectionFragment();
        //Bundle arg = new Bundle();
        //arg.putSerializable(ARG_PLAYER_NAME, nameArg);
        //choreListFragment.setArguments(arg);
        return playerSelectionFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserId = mFirebaseUser.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        final View view = inflater.inflate(R.layout.fragment_player_selection, container, false);

        final ArrayList<Player> players = DataUtil.loadAllPlayerData(getActivity());
        if (players == null) {

            // Load player info
            mDatabase.child("users").child(mUserId).child("Players").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() != null) {
                        //user exists, do something
                        for(final DataSnapshot dsp : snapshot.getChildren()){
                            mGroupMembers.add(String.valueOf(dsp.getKey())); // player names
                            mPlayers.add(new Player(String.valueOf(dsp.getKey())));
                            ArrayList<Chore> chosenChores = new ArrayList<>();
                            for(DataSnapshot ds : dsp.child("Chore Lists").getChildren()) {
                                int chorePoints = Integer.parseInt(ds.getValue().toString());
                                Chore chore = new Chore(ds.getKey(), chorePoints);
                                chosenChores.add(chore);
                                for (int i = 0; i < mPlayers.size(); i++) {
                                    if (mPlayers.get(i).getmPlayerName().equals(String.valueOf(dsp.getKey()))) {
                                        mPlayers.get(i).setmChoreList(chosenChores);
                                    }
                                }
                            }
                        }

                        DataUtil.saveAllPlayerData(mPlayers, getActivity());

                        ViewGroup linearLayout = (ViewGroup) view.findViewById(R.id.fragment_player_selection);

                        for (int i = 0; i < mPlayers.size(); i++) {
                            Button button = new Button(getActivity());
                            button.setText(mPlayers.get(i).getmPlayerName());
                            //FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(200, 200);
                            button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            button.setBackgroundResource(R.drawable.button_background_with_shadow);
                            button.setBackground(getResources().getDrawable(R.drawable.button_background_with_shadow, null));
                            button.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_background_with_shadow, null));
                            button.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_background_with_shadow));
                            button.setBackgroundResource(R.drawable.button_background_with_shadow);
                            button.setTextColor(Color.WHITE);
                            linearLayout.addView(button);

                            final int iterator = i;

                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    Intent intent = new Intent(getActivity(), MainMenuActivity.class);
                                    intent.putExtra(EXTRA_PLAYER, mPlayers.get(iterator).getmPlayerName());
                                    startActivity(intent);
                                }
                            });
                        }
                    } else {
                        //user does not exist, do something else
                    }
                }
                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("Failed to read value.", error.toException());
                }
            });
        } else {
            mPlayers = players;
            DataUtil.saveAllPlayerData(mPlayers, getActivity());
        }

        ViewGroup linearLayout = (ViewGroup) view.findViewById(R.id.fragment_player_selection);

        // Dynamically create buttons for players
        for (int i = 0; i < mPlayers.size(); i++) {
            Button button = new Button(getActivity());
            button.setText(mPlayers.get(i).getmPlayerName());
            button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            button.setBackgroundResource(R.drawable.button_background_with_shadow);
            button.setBackground(getResources().getDrawable(R.drawable.button_background_with_shadow, null));
            button.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_background_with_shadow, null));
            button.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_background_with_shadow));
            button.setBackgroundResource(R.drawable.button_background_with_shadow);
            button.setTextColor(Color.WHITE);
            linearLayout.addView(button);

            final int iterator = i;

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), MainMenuActivity.class);
                    intent.putExtra(EXTRA_PLAYER, mPlayers.get(iterator).getmPlayerName());
                    startActivity(intent);
                }
            });
        }

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();

            ArrayList<Chore> chores = new ArrayList<>();
            chores.add(new Chore("Clean the Litter Box", 4));
            chores.add(new Chore("Dishes", 2));
            chores.add(new Chore("Dust", 2));
            chores.add(new Chore("Laundry", 2));
            chores.add(new Chore("Organize", 2));
            chores.add(new Chore("Paint the Floor", 8));
            chores.add(new Chore("Rake Leaves", 6));
            chores.add(new Chore("Shovel Snow", 6));
            chores.add(new Chore("Vacuum", 4));
            chores.add(new Chore("Walk the Dog", 4));
            chores.add(new Chore("Wash Dishes", 2));

            // Reset Chore List
            DataUtil.saveAllChoreData(chores, getActivity());

            // Reset Saved Player Data
            DataUtil.saveAllPlayerData(null, getActivity());

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

