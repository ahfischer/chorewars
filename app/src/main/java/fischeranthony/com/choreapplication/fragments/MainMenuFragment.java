package fischeranthony.com.choreapplication.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import fischeranthony.com.choreapplication.R;
import fischeranthony.com.choreapplication.activities.ChoreListActivity;
import fischeranthony.com.choreapplication.activities.LoginActivity;
import fischeranthony.com.choreapplication.activities.MainMenuActivity;
import fischeranthony.com.choreapplication.activities.PlayerStatsActivity;
import fischeranthony.com.choreapplication.activities.ResultsActivity;
import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.utils.DataUtil;

public class MainMenuFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "MainMenuFragment.TAG";

    private static final String ARG_PLAYER_NAME = "com.fullsail.android.ARG_PLAYER_NAME";

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mDatabase;
    private String mUserId;

    private String mPlayerName;

    public static MainMenuFragment newInstance(String nameArg) {
        MainMenuFragment mainMenuFragment = new MainMenuFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(ARG_PLAYER_NAME, nameArg);
        mainMenuFragment.setArguments(arg);
        return mainMenuFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserId = mFirebaseUser.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        Bundle arguments = getArguments();
        if (arguments != null) {

            mPlayerName = arguments.getString(ARG_PLAYER_NAME);

            view.findViewById(R.id.dailyRecordButton).setOnClickListener(this);
            view.findViewById(R.id.weeklyRecordButton).setOnClickListener(this);
            view.findViewById(R.id.choreListButton).setOnClickListener(this);
            view.findViewById(R.id.playerStatsButton).setOnClickListener(this);

        }

        return view;
    }

    // Open all menu options to proper activities
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dailyRecordButton) {
            Intent intent = new Intent(getActivity(), ResultsActivity.class);
            startActivity(intent);
        }
        if (v.getId() == R.id.weeklyRecordButton) {
            Intent intent = new Intent(getActivity(), ResultsActivity.class);
            intent.putExtra(PlayerSelectionFragment.EXTRA_PLAYER, mPlayerName);
            startActivity(intent);
        }
        if (v.getId() == R.id.choreListButton) {
            Intent intent = new Intent(getActivity(), ChoreListActivity.class);
            intent.putExtra(PlayerSelectionFragment.EXTRA_PLAYER, mPlayerName);
            startActivity(intent);
        }
        if (v.getId() == R.id.playerStatsButton) {
            Intent intent = new Intent(getActivity(), PlayerStatsActivity.class);
            intent.putExtra(PlayerSelectionFragment.EXTRA_PLAYER, mPlayerName);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();

            ArrayList<Chore> chores = new ArrayList<>();
            chores.add(new Chore("Clean the Litter Box", 4));
            chores.add(new Chore("Dishes", 2));
            chores.add(new Chore("Dust", 2));
            chores.add(new Chore("Laundry", 2));
            chores.add(new Chore("Organize", 2));
            chores.add(new Chore("Paint the Floor", 8));
            chores.add(new Chore("Rake Leaves", 6));
            chores.add(new Chore("Shovel Snow", 6));
            chores.add(new Chore("Vacuum", 4));
            chores.add(new Chore("Walk the Dog", 4));
            chores.add(new Chore("Wash Dishes", 2));

            // Reset Chore List
            DataUtil.saveAllChoreData(chores, getActivity());

            // Reset Saved Player Data
            DataUtil.saveAllPlayerData(null, getActivity());

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            getActivity().finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
