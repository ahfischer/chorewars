package fischeranthony.com.choreapplication.objects;

import java.io.Serializable;

public class Chore implements Serializable {

    String mChoreName;
    int mChorePoints;

    public Chore(String mChoreName, int mChorePoints) {
        this.mChoreName = mChoreName;
        this.mChorePoints = mChorePoints;
    }

    public String getmChoreName() {
        return mChoreName;
    }

    public int getmChorePoints() {
        return mChorePoints;
    }
}
