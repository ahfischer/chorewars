package fischeranthony.com.choreapplication.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class Player implements Serializable {

    String mPlayerName;
    ArrayList<Chore> mChoreList;
    ArrayList<Chore> mCompletedChores;
    int mAvatar;
    int mPoints = 0;


    public Player(String mPlayerName) {
        this.mPlayerName = mPlayerName;
    }

    public Player(String mPlayerName, ArrayList<Chore> mChoreList, ArrayList<Chore> mCompletedChores) {
        this.mPlayerName = mPlayerName;
        this.mChoreList = mChoreList;
        this.mCompletedChores = mCompletedChores;
    }

    public Player(String mPlayerName, ArrayList<Chore> mChoreList, ArrayList<Chore> mCompletedChores, int mAvatar) {
        this.mPlayerName = mPlayerName;
        this.mChoreList = mChoreList;
        this.mCompletedChores = mCompletedChores;
        this.mAvatar = mAvatar;
    }

    public String getmPlayerName() {
        return mPlayerName;
    }

    public ArrayList<Chore> getmChoreList() {
        return mChoreList;
    }

    public ArrayList<Chore> getmCompletedChores() {
        return mCompletedChores;
    }

    public int getmAvatar() {
        return mAvatar;
    }

    public int getmPoints() {
        return mPoints;
    }

    public void setmPlayerName(String mPlayerName) {
        this.mPlayerName = mPlayerName;
    }

    public void setmChoreList(ArrayList<Chore> mChoreList) {
        this.mChoreList = mChoreList;
    }

    public void setmCompletedChores(ArrayList<Chore> mCompletedChores) {
        this.mCompletedChores = mCompletedChores;
    }

    public void setmAvatar(int mAvatar) {
        this.mAvatar = mAvatar;
    }

    public void setmPoints(int mPoints) {
        this.mPoints = mPoints;
    }

    //public void addChore(String chore) {
        //this.mChoreList.add(chore);
    //}
}
