package fischeranthony.com.choreapplication.utils;

import android.app.Activity;
import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import fischeranthony.com.choreapplication.objects.Chore;
import fischeranthony.com.choreapplication.objects.Player;

public class DataUtil {
    private static final String ALL_PLAYER_DATA = "All_Player_Data.txt";
    private static final String ALL_CHORE_DATA = "All_Chore_Data.txt";

    public static void saveAllPlayerData(ArrayList<Player> playerData, Context context) {

        try {
            FileOutputStream fos = context.getApplicationContext().openFileOutput(ALL_PLAYER_DATA, Activity.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(playerData);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveAllChoreData(ArrayList<Chore> choreData, Context context) {

        try {
            FileOutputStream fos = context.getApplicationContext().openFileOutput(ALL_CHORE_DATA, Activity.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(choreData);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Player> loadAllPlayerData(Context context) {

        ArrayList<Player> data = null;
        try {
            FileInputStream fis = context.openFileInput(ALL_PLAYER_DATA);
            ObjectInputStream ios = new ObjectInputStream(fis);
            data = (ArrayList<Player>)ios.readObject();
            ios.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static ArrayList<Chore> loadAllChoreData(Context context) {

        ArrayList<Chore> data = null;
        try {
            FileInputStream fis = context.openFileInput(ALL_CHORE_DATA);
            ObjectInputStream ios = new ObjectInputStream(fis);
            data = (ArrayList<Chore>)ios.readObject();
            ios.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

        return data;
    }
}
